# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)

function impares_consecutivos(n)
	k=n^3
	impar=1
	soma=0
	while soma!=k
		soma=0
		for i in 1:n
			soma=soma+impar+2*(i-1)
		end
		impar=impar+2
	end
	return impar-2
end

# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function imprime_impares_consecutivos(m)
        k=m^3
        impar=1
        soma=0
        while soma!=k
                soma=0
                for i in 1:m
                        soma=soma+impar+2*(i-1)
                end
                impar=impar+2
        end
        print(m," ")
	print(k," ")
	for i in 1:m
		print((impar-2)," ")
		impar=impar+2
	end
end

function mostra_n(n)
	for i in 1:n
		imprime_impares_consecutivos(i)
		println("")
	end
end

# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        print("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        print("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        print("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        print("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        print("Sua função retornou o valor errado para n = 21")
    end
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
test()
mostra_n(20)
